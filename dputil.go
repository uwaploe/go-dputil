// Package contains various utility functions and data types for the Deep
// Profiler project
package dputil

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
)

// Convert time.Time instance to a data timestamp
func timeToTimestamp(t time.Time) (secs int32, usecs int32) {
	secs = int32(t.Unix())
	usecs = int32(t.Nanosecond() / 1000)
	return
}

func toInt64(raw interface{}) int64 {
	v := reflect.ValueOf(raw)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return int64(v.Uint())
	}

	return 0
}

func timeFromVals(secs interface{}, usecs interface{}) time.Time {
	// The serialized encoding of the time-stamp values can vary
	return time.Unix(toInt64(secs), toInt64(usecs))
}

// DataRecord represents a single sample acquired by one of the
// Deep Profiler sensors.
type DataRecord struct {
	// Data source (sensor name)
	Source string
	// Acquisition time
	T time.Time
	// Data value, either a binary string or
	// a map of variable names and values.
	Data interface{}
}

// When DataRecord.Data is a map, it should be of the following type
var DataRecordMapType reflect.Type = reflect.TypeOf(map[string]interface{}{})

// Return the Data value as a map or nil
func (r DataRecord) AsMap() map[string]interface{} {
	if m, ok := r.Data.(map[string]interface{}); ok {
		return m
	} else {
		return nil
	}
}

// Return the Data value as a string
func (r DataRecord) AsString() string {
	if m, ok := r.Data.(string); ok {
		return m
	} else {
		return ""
	}
}

// Return the Data value as a byte slice or nil
func (r DataRecord) AsBytes() []byte {
	if m, ok := r.Data.([]byte); ok {
		return m
	} else {
		return []byte{}
	}
}

// Return the structure fields as a list of empty interfaces
func (r DataRecord) Serialize() []interface{} {
	v := make([]interface{}, 4)

	v[0] = r.Source
	v[1], v[2] = timeToTimestamp(r.T)
	v[3] = r.Data

	return v
}

// String implements the fmt.Stringer interface.
func (r DataRecord) String() string {
	b, _ := r.MarshalText()
	return string(b)
}

// MarshalText implements the encoding.TextMarshaler interface
func (r DataRecord) MarshalText() ([]byte, error) {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s %s ", r.T.Format(time.RFC3339), r.Source)
	m := r.AsMap()

	if m != nil {
		keys := make([]string, 0, len(m))
		for k := range m {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		for _, k := range keys {
			fmt.Fprintf(&buf, "%s=%v ", k, m[k])
		}
	} else {
		fmt.Fprintf(&buf, "%s", r.AsString())
	}

	return buf.Bytes(), nil
}

// Create a new DataRecord instance from a list of empty
// interfaces.
func Deserialize(v ...interface{}) DataRecord {
	dr := DataRecord{}
	di := len(v) - 1
	switch len(v) {
	case 4:
		dr.Source = v[0].(string)
		dr.T = timeFromVals(v[1], v[2])
	case 3:
		dr.T = timeFromVals(v[0], v[1])
	default:
		return dr
	}

	dr.Data = v[di]
	return dr
}

// MarshalJSON implements the json.Marshaler interface
func (r DataRecord) MarshalJSON() ([]byte, error) {
	secs, usecs := timeToTimestamp(r.T)
	obj := map[string]interface{}{
		"name": r.Source,
		"t":    []int32{secs, usecs},
	}

	switch data := r.Data.(type) {
	case map[string]interface{}:
		obj["data"] = data
	case string:
		obj["data"] = data
	default:
		return nil, fmt.Errorf("Unsupported data type: %T", r.Data)
	}

	return json.Marshal(obj)
}

// UnmarshalJSON implements the json.Unmarshaler interface
func (r *DataRecord) UnmarshalJSON(s []byte) error {
	obj := struct {
		Name string          `json:"name"`
		T    []int           `json:"t"`
		Data json.RawMessage `json:"data"`
	}{}

	err := json.Unmarshal(s, &obj)
	if err != nil {
		return err
	}
	r.Source = obj.Name
	r.T = time.Unix(int64(obj.T[0]), int64(obj.T[1])*1000)

	var val interface{}
	if err := json.Unmarshal(obj.Data, &val); err != nil {
		return err
	}

	switch data := val.(type) {
	case string:
		r.Data = data
	case map[string]interface{}:
		r.Data = data
	default:
		return fmt.Errorf("Unsupported data type: %T", val)
	}
	return nil
}

// Event represents an event message from the DPC
type Event struct {
	Name  string
	Attrs map[string]interface{}
	desc  string
}

// Parse an event message.
// An event message is a string of the form:
//
//    EVENTNAME KEY=VALUE KEY=VALUE ...
//
func ParseEvent(rawtext string) Event {
	var err error
	var ival int64

	ev := Event{}
	fields := strings.Split(strings.Trim(rawtext, " \t\r\n"), " ")
	ev.Name = strings.ToLower(fields[0])
	ev.Attrs = make(map[string]interface{})
	for _, v := range fields[1:] {
		f := strings.Split(v, "=")
		ival, err = strconv.ParseInt(f[1], 0, 64)
		if err == nil {
			ev.Attrs[f[0]] = ival
		} else {
			if parts := strings.Split(f[1], ","); len(parts) > 1 {
				ev.Attrs[f[0]] = parts
			} else {
				ev.Attrs[f[0]] = f[1]
			}
		}
	}

	return ev
}

// SetDescription adds a description to an Event. The description will be
// included in the JSON serialization.
func (ev Event) SetDescription(desc string) {
	ev.desc = desc
}

// String implements the fmt.Stringer interface
func (ev Event) String() string {
	keys := make([]string, 0, len(ev.Attrs))
	for k := range ev.Attrs {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var b strings.Builder
	fmt.Fprint(&b, ev.Name)
	for _, k := range keys {
		if v, ok := ev.Attrs[k].([]string); ok {
			fmt.Fprintf(&b, " %s=%s", k, v[0])
			for _, elem := range v[1:] {
				fmt.Fprintf(&b, ",%s", elem)
			}
		} else {
			fmt.Fprintf(&b, " %s=%v", k, ev.Attrs[k])
		}
	}
	return b.String()
}

// MarshalText implements the encoding.TextMarshaler interface
func (ev Event) MarshalText() ([]byte, error) {
	return []byte(ev.String()), nil
}

// Return a JSON representation of an Event with a description
func (ev Event) ToJson(desc string) ([]byte, error) {
	ev.SetDescription(desc)
	return json.Marshal(ev)
}

// MarshalJSON implements the json.Marshaler interface
func (ev Event) MarshalJSON() ([]byte, error) {
	obj := map[string]interface{}{"name": ev.Name}
	d := map[string]interface{}{}

	if ev.desc != "" {
		obj["desc"] = ev.desc
	}

	for k, v := range ev.Attrs {
		switch k {
		case "t":
			obj["t"] = []int32{int32(v.(int64)), 0}
		default:
			d[k] = v
		}
	}
	obj["attrs"] = d
	return json.Marshal(obj)
}
