package dputil

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"
)

func TestEventParse(t *testing.T) {
	s1 := string("profile:start list=fe,fi,fo mode=up pnum=42 t=123456789")
	ev := ParseEvent(s1)

	v, ok := ev.Attrs["pnum"].(int64)
	if !ok || v != 42 {
		t.Errorf("Incorrect int parsing: %v", ev)
	}

	m, ok := ev.Attrs["mode"].(string)
	if !ok || m != "up" {
		t.Errorf("Incorrect string parsing: %v", ev)
	}

	l, ok := ev.Attrs["list"].([]string)
	if !ok || !reflect.DeepEqual(l, []string{"fe", "fi", "fo"}) {
		t.Errorf("Incorrect list parsing: %v", ev)
	}

	if ev.String() != s1 {
		t.Errorf("Incorrect string representation: %q", ev.String())
	}

	ev2 := ParseEvent(ev.String())
	if ev2.Attrs["mode"] != ev.Attrs["mode"] {
		t.Errorf("String conversion failed: %v", ev2)
	}

	b, err := ev.ToJson("")
	if err != nil {
		t.Errorf("event.ToJson error: %v", err)
	}
	t.Logf("JSON event format: %s", b)
}

func TestDataRecord(t *testing.T) {
	s := make(map[string]interface{})
	s["condwat"] = 33.1234
	s["tempwat"] = 10.1
	s["preswat"] = 42.0
	tstamp := time.Now()
	dr := DataRecord{Source: "ctd_1", T: tstamp, Data: s}
	vals := dr.Serialize()
	if len(vals) != 4 {
		t.Errorf("DataRecord serialization failed: %v", vals)
	}

	secs, ok := vals[1].(int32)
	if !ok || secs != int32(tstamp.Unix()) {
		t.Errorf("Improper time conversion: %v", vals[1])
	}

	dr2 := Deserialize(vals...)

	s2, ok := dr2.Data.(map[string]interface{})
	if !ok {
		t.Error("Bad data format")
	}
	if s2["tempwat"] != s["tempwat"] {
		t.Errorf("Data mismatch: %v != %v", s2["tempwat"], s["tempwat"])
	}
}

func TestJsonConversion(t *testing.T) {
	s := make(map[string]interface{})
	s["condwat"] = 33.1234
	s["tempwat"] = 10.1
	s["preswat"] = 42.0
	s["mode"] = "foo"
	tstamp := time.Now()
	dr := DataRecord{Source: "ctd_1", T: tstamp, Data: s}
	js, err := json.Marshal(dr)
	if err != nil {
		t.Errorf("ToJson error: %v", err)
	}

	dr2 := DataRecord{}
	err = json.Unmarshal(js, &dr2)
	if err != nil {
		t.Errorf("FromJson error: %v", err)
	}

	s2, ok := dr2.Data.(map[string]interface{})
	if !ok {
		t.Error("Bad data format")
	}
	if s2["tempwat"] != s["tempwat"] {
		t.Errorf("Data mismatch: %v != %v", s2["tempwat"], s["tempwat"])
	}
}

func TestNonMapData(t *testing.T) {
	dr := DataRecord{
		Source: "evlogger",
		T:      time.Now(),
		Data:   "testing 1 2 3 4",
	}

	m := dr.AsMap()
	if m != nil {
		t.Errorf("Invalid response: %v", m)
	}

	s := dr.AsString()
	if s == "" {
		t.Errorf("String conversion failed")
	}

	dr.Data = []byte("hello world")
	b := dr.AsBytes()
	if len(b) == 0 {
		t.Errorf("Bytes conversion failed")
	}
}
